package weather;

import model.Forecasts;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.client.RestTemplate;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;

@Controller
public class WeatherController {

	@GetMapping("/")
	public String index(Model model) throws JAXBException {
		RestTemplate template = new RestTemplate();

		String response = template
				.getForObject("https://ilmateenistus.ee/ilma_andmed/xml/forecast.php?lang=eng", String.class);

		if (response == null) {
			return "index";
		}

		StringReader sr = new StringReader(response);
		JAXBContext jaxbContext = JAXBContext.newInstance(Forecasts.class);
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		Forecasts forecasts = (Forecasts) unmarshaller.unmarshal(sr);

		model.addAttribute("forecasts", forecasts);

		return "index";
	}
}