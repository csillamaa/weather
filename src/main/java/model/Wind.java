package model;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.util.Map;

@Data
@XmlAccessorType(XmlAccessType.FIELD)
public class Wind {
    String name;
    String direction;

    @XmlElement(name = "speedmin")
    int minSpeed;
    @XmlElement(name = "speedmax")
    int maxSpeed;

    String gust;

    public String toDataString() {
        Map<String, String> directions = Map.of(
                "North wind", "N",
                "South wind", "S",
                "West wind", "W",
                "East wind", "E",
                "Northwest wind", "NW",
                "Southwest wind", "SW",
                "Northeast wind", "NE",
                "Southeast wind", "SE"
        );
        return directions.get(direction) + " " + minSpeed + " .." + maxSpeed + "m/s";
    }

    public String toSummaryString() {
        return name + " - " + direction + (gust.isEmpty() ? "" : ". Gust - " + gust + "m/s" );
    }
}
