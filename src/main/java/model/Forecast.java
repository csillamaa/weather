package model;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

@Data
@XmlAccessorType(XmlAccessType.FIELD)
public class Forecast {
    @XmlAttribute
    String date;

    Night night;
    Day day;

    public String toDateString() {
        String[] dateList = date.split("-");
        return dateList[1] + "." + dateList[2];
    }
}
