package model;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.util.List;

@Data
@XmlAccessorType(XmlAccessType.FIELD)
public abstract class PartOfDay {
    String phenomenon;

    @XmlElement(name = "tempmax")
    String maxTemp;
    @XmlElement(name = "tempmin")
    String minTemp;

    String text;
    String sea;
    String peipsi;

    @XmlElement(name = "place")
    List<Place> places;
    @XmlElement(name = "wind")
    List<Wind> winds;

    public String toSummaryString(){
        return phenomenon + ". " + text;
    }

    public String toDegreeString(){
        return minTemp + " .." + maxTemp;
    }
}
