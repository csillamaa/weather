package model;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@Data
@XmlAccessorType(XmlAccessType.FIELD)
public class Place {
    String name;
    String phenomenon;

    @XmlElement(name = "tempmin")
    String minTemp;
    @XmlElement(name = "tempmax")
    String maxTemp;

    public String toSummaryString() {
        return name + " - " + phenomenon;
    }

    public String toDataString() {
        return  (maxTemp == null ? minTemp : maxTemp) + "°C";
    }
}
